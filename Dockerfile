FROM maven:3.6

WORKDIR /home/app

COPY complete /home/app/

RUN mvn clean package

EXPOSE 8071

ENTRYPOINT java -jar complete/target/*.jar
